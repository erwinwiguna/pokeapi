import React from 'react'
import Link from 'next/link'
import $ from 'jquery'
import axios from 'axios'
import Head from '../components/head'
import Header from '../components/header'
import Footer from '../components/footer'
import firebase from '../Firebase'
import Collapse, { Panel } from 'rc-collapse'
import PopupRelease from '../components/popup/popup-release'
import PopupNotification from '../components/popup/popup-notification'

export default class extends React.Component {
	static async getInitialProps({ req, res, query }) {
		return { query }
	}

	constructor(props) {
		super(props)

		this.state = {
			id: this.props.query.id,
			key: this.props.query.key,
			title: 'My Pokemon Detail',
			pokemonmoves: [],
			pokemonsprites: [],
			pokemontypes: [],
			pokemonname: [],
			nickname: '',
			height: 0,
			weight: 0,
			isData: false,
			isNotif: false,
			isError: false,
			message: '',
			is404: false,
		}

		this.handleNotification = this.handleNotification.bind(this)
	}

	componentDidMount() {
		let ref = firebase.firestore().collection('mypokemon').doc(this.state.key)

		ref.get().then((doc) => {
			console.log(doc.data())
	    	if (doc.exists) {
	        	this.setState({
	          		pokemonname: doc.data().pokemonname,
	          		weight: doc.data().weight,
	          		height: doc.data().height,
	          		nickname: doc.data().nickname,
	          		pokemonmoves: doc.data().pokemonmoves,
	          		pokemontypes: doc.data().pokemontypes,
	          		pokemonsprites: doc.data().pokemonsprites,
	          		isData: true,
	        	});
	      	} else {
	        	this.handleNotification(true, true, true, 'Cannot find the pokemon')
	      	}
	    });
	}

	handleNotification(isnotif, iserror, is404='false', message) {
		this.setState({
			isError: iserror,
			isNotif: isnotif,
			message: message,
			is404: is404,
		})
	}
	
	render() {
		return (
			<React.Fragment>
				<Head />
				
				<div className="container">
					<Header selectedMenu="mypokemon" />
				</div>
				
				<main role="main" className="container">
					<div className="row">
						<div className="col-12 blog-main">
							<div className="blog-post">
								<h4 className="blog-post-title">My Pokemon Detail</h4>
								<hr />
								<div className="col-12">
									<div className="row no-gutters border rounded overflow-hidden flex-md-row mb-4 shadow-sm h-md-250 position-relative">
										<div className="col-12 bg-secondary p-2">
											<h4 className="mb-0 text-capitalize text-white">{this.state.nickname}</h4>
										</div>
										<div className="col-12">
											<div className="row mx-1">
												<div className="col-6 p-1 d-flex flex-column position-static justify-content-center">
													<h5 className="mb-0 text-capitalize">{this.state.pokemonname}</h5>
													<p className="mb-0"><small>Height: {this.state.height} m</small></p>
													<p className="mb-0"><small>Weight: {this.state.weight} kg</small></p>
												</div>
												{	this.state.pokemonsprites &&
													<div className="col-6 p-0 text-right">
														<img src={this.state.pokemonsprites.front_default} alt={this.state.pokemonname} className="p-1" />
													</div>
												}
											</div>
										</div>
									</div>
								</div>

								{	this.state.pokemontypes &&
									<Collapse accordion={true}>
									    <Panel header="Types" headerClass="my-header-class">
									    	<ul>
									    	{
									    		this.state.pokemontypes.map((data, i) => {
									    			return(
									    				<li key={i}>{data.type.name}</li>
									    			)
									    		})
									    	}
									    	</ul>
									    </Panel>
									    <Panel header="Moves">
									    	<ul>
									    		{
									    			this.state.pokemonmoves.map((data, i) => {
									    				return(
									    					<li key={i}>{data.move.name}</li>
									    				)
									    			})
									    		}
									    	</ul>
									    </Panel>
									</Collapse>
								}
							</div>
							<div className="row m-2">
								<button type="button" className="btn btn-danger col-12 popup-release">Release</button>
							</div>
						</div>
					</div>
				</main>

				{
					this.state.isData &&
					<PopupRelease handleNotification={this.handleNotification} firebasekey={this.state.key} pokemonid={this.state.id} />
				}
				
				{
					this.state.isNotif &&
					<PopupNotification isError={this.state.isError} message={this.state.message} is404={this.state.is404} isMyPokemon={true} />
				}
			</React.Fragment>
		)
	}
}