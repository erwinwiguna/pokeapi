import React from 'react'
import Link from 'next/link'
import Router from 'next/router'

export default class extends React.Component {
	constructor(props) {
		super(props)
		
		this.state = {
			login: false
		}
		
		this.logout = this.logout.bind(this)
	}
	
	componentDidMount() {
		let user = localStorage.getItem('user')
		if (user) {
			this.setState({
				login: true
			})
		}
	}
	
	logout() {
		localStorage.removeItem('user')
		Router.push({
			pathname: '/api/logout'
		})
	}
	
	render() {
		return (
			<React.Fragment>
				<header className="blog-header py-3">
					<div className="row flex-nowrap justify-content-between align-items-center">
						<div className="col-12 text-center">
							<a className="blog-header-logo text-dark" href="#">PokeApi</a>
						</div>
					</div>
				</header>
				
				<nav className="nav d-flex justify-content-between">
					<Link href="/" as="/" passHref>
						<a href="javascript:void(0);" className={this.props.selectedMenu == 'home' ? "p-2 text-primary font-weight-bold" : "p-2 text-muted"}>Home</a>
					</Link>
					<Link href="/pokemon" as="/pokemon" passHref>
						<a href="javascript:void(0);" className={this.props.selectedMenu == 'pokemonlist' ? "p-2 text-primary font-weight-bold" : "p-2 text-muted"}>Pokemon List</a>
					</Link>
					<Link href="/my-pokemon" as="/my-pokemon" passHref>
						<a href="javascript:void(0);" className={this.props.selectedMenu == 'mypokemon' ? "p-2 text-primary font-weight-bold" : "p-2 text-muted"}>My Pokemon</a>
					</Link>
				</nav>
				
				<hr />
			</React.Fragment>
		)
	}
}