import Link from 'next/link'

const Footer = (props) => (
	<React.Fragment>
		<footer className="blog-footer">
			<p>TOKOPEDIA Project Assignment by Erwin Wiguna.</p>
			<p><a href="#">Back to top</a></p>
		</footer>
	</React.Fragment>
)

export default Footer