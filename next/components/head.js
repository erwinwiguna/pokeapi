import NextHead from 'next/head'
import { string } from 'prop-types'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'rc-collapse/assets/index.css'

const defaultTitle = 'PokeApi'
const defaultDescription = 'This website provides a RESTful API interface to highly detailed objects built from thousands of lines of data related to Pokémon.'
const defaultKeyword = 'pokeapi, pokemon'

const Head = (props) => (
	<NextHead>
		<meta charSet="UTF-8" />
		<title>{props.title || defaultTitle}</title>
		<meta name="description" content={props.description || props.title ? props.title + ' - ' + defaultDescription : defaultDescription} />
		<meta name="keywords" content={props.keywords || defaultKeyword} />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
		<meta name="theme-color" content="#ffffff" />
		
		<link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet" />
		<link href="/static/css/main.css" rel="stylesheet" />

		<link rel="apple-touch-icon" href="/static/images/logo-129x129.png" />
		<link rel="apple-touch-icon" sizes="129x129" href="/static/images/logo-129x129.png" />
		<link rel="apple-touch-icon" sizes="114x114" href="/static/images/logo-114x114.png" />
		<link rel="apple-touch-icon" sizes="120x120" href="/static/images/logo-120x120.png" />		
		<link rel="apple-touch-icon" sizes="152x152" href="/static/images/logo-152x152.png" />
		
		<link rel="shortcut icon" href="/static/images/favicon.png" />
		<link rel="manifest" href="/manifest.json" />

		<script src="/static/js/main.js"></script>
		<script src="https://www.gstatic.com/firebasejs/6.5.0/firebase-app.js"></script>
	</NextHead>
)

Head.propTypes = {
	title: string,
	description: string,
	keyword: string,
}

export default Head