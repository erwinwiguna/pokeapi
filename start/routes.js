'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const Next = use('Adonis/Addons/Next')
const handler = Next.getRequestHandler()

Route.get('/pokemon', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/pokemon-list', query)
})

Route.get('/my-pokemon', ({ request, response }) => {
	const query = request.get()
	return Next.render(request.request, response.response, '/my-pokemon', query)
})

Route.get('/pokemon/detail/:id', ({ params, request, response }) => {
	let qparams = {
		id: params.id
	}
	const query = qparams
	return Next.render(request.request, response.response, '/pokemon-detail', query)
})

Route.get('/my-pokemon/detail/:id/:key', ({ params, request, response }) => {
	let qparams = {
		id: params.id,
		key: params.key
	}
	const query = qparams
	return Next.render(request.request, response.response, '/my-pokemon-detail', query)
})

Route.get('*', ({ request, response }) =>
	new Promise((resolve, reject) => {
		handler(request.request, response.response, promise => {
			promise.then(resolve).catch(reject)
		})
	})
)
